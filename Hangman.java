import java.util.Scanner;
public class Hangman{
	public static int isLetterInWord(String word, char c){
		for (int i = 0; i<4; i++){
			if (word.charAt(i) == c){
				return i;
			}
		}
		return -1;
	}
	public static char toUpperCase(char c){
		c = Character.toUpperCase(c);
		return c;
	}
	public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3){
		String theWord = "";
		
		if (letter0 == true){ //If the letter at position 0 is true, the string "theWord" will get updated.
			theWord += word.charAt(0);
		}
		else { //If it is false, this position will be replace with a line to show the user that it was not guessed correctly.
			theWord += "_";
		}
		
		if (letter1 == true){
			theWord += word.charAt(1);
		}
		else {
			theWord += "_";
		}
		
		if (letter2 == true){
			theWord += word.charAt(2);
		}
		else {
			theWord += "_";
		}
		
		if (letter3 == true){
			theWord += word.charAt(3);
		}
		else {
			theWord += "_";
		}
		System.out.println("Your result is " + theWord);
	}
	public static void runGame(String word){
		boolean letter0 = false;
		boolean letter1 = false;
		boolean letter2 = false;
		boolean letter3 = false;
		int numberOfGuesses = 0;
		
		while (numberOfGuesses < 6 && !(letter0 && letter1 && letter2 && letter3)){

			Scanner reader = new Scanner(System.in);
			char c = reader.nextLine().charAt(0);
			c = toUpperCase(c);
			

			if(isLetterInWord(word, c) == 0){
				letter0 = true; 
			}
			if(isLetterInWord(word, c) == 1){
				letter1 = true;
			}
			if(isLetterInWord(word, c) == 2){
				letter2 = true;
			}
			if(isLetterInWord(word, c) == 3){
				letter3 = true;
			}
			if(isLetterInWord(word, c) == -1){
				numberOfGuesses++;
			}
			printWork(word, letter0, letter1, letter2, letter3); 
		}
		if(numberOfGuesses>=6){
			System.out.println("Nice try ! But you did not successfully guess the word! :(");
		}
		else{
			System.out.println("Congrats! You successfully guessed the word! :)");
		}
	}
}