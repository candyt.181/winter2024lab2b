import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;
public class wordle{
	public static String generateWord(){
		String[] listWords = new String[]{"alone","arise","brain","brief","court","cause","doubt","dance","early","entry","fruit","float","grade","ghost","heart","human","ideal","image","judge","juice"};
		Random random = new Random();
		
		int x = random.nextInt(listWords.length);
		
		return listWords[x];
	}

	public static boolean letterInWord(String word, char letter){
		for(int i =0; i<word.length();i++){
			if(word.charAt(i) == letter){
				return true;
			}
		}
		return false;	
	}


	public static boolean letterInSlot(String word, char letter, int position){
			if (word.charAt(position) == letter){
				return true;
			}
		
		return false;
	}


	public static String[] guessWord(String answer, String userGuess){
		String[] colourCodes = new String[]{"white", "white", "white", "white", "white"};
		
		
		for(int i =0; i<answer.length(); i++){
			char letter = userGuess.charAt(i);
			if((letterInWord(answer, letter) ==true) && letterInSlot(answer, letter, i) ==true){
				colourCodes[i] = "green"; 
			}
			else if(letterInWord(answer, letter) == true){
				colourCodes[i] = "yellow";
			}
			else if((letterInWord(answer, letter) ==false) && letterInSlot(answer, letter, i) ==false){
				colourCodes[i] = "white";
			}
		}
		return colourCodes;
	}


	public static void presentResults(String word, String[] colours){
		
		final String ANSI_RESET = "\u001B[0m"; 
		final String ANSI_YELLOW = "\u001B[33m";
		final String ANSI_GREEN = "\u001B[32m";	
		
		for(int i=0; i<word.length(); i++){
			if(colours[i] == "green"){
				System.out.println(ANSI_GREEN + word.charAt(i) + ANSI_RESET);
			}
			if(colours[i] == "yellow"){
				System.out.println(ANSI_YELLOW + word.charAt(i)+ ANSI_RESET);
			}
		}
	}
	
	
	public static String readGuess(){
		Scanner reader = new Scanner(System.in);
		System.out.println("Please input a guess word :)");
		String word = reader.nextLine();
		if(word.length() !=5){
			return "The word input is not 5 letters. Please input a guess word again.";
		}
		else{
			return word.toUpperCase();
		}
		
    }
	
	public static void runGame(String word){
	
			boolean goodAnswer = false;
			int numberOfGuesses = 0; 
		
			while (numberOfGuesses < 6 && goodAnswer == false){
				String guess = readGuess();
				String[] colours = guessWord(word.toUpperCase(), guess);
			
				System.out.println("Here's what you got right so far: ");
				presentResults(guess, colours);
			
				if(guess.equals(word.toUpperCase())){
					goodAnswer = true;
					System.out.println("Congrats ! You win ! You successfully guessed the word: " + word + " :)");
				}
				else{
					numberOfGuesses++;
					System.out.println("Incorrect !  Please try again :(");
				}
			
			}
			if(goodAnswer == false){
				System.out.println("Uh oh ! You did not successfully guess the word: " + word + ". Play again or exit the game.");
			}
		
	}
}
