import java.util.Scanner;
public class GameLauncher{
	public static void main(String[] args){ 
		//Asking the user what game they want to play. Either press 1 for Hangman, or press 2 for Wordle.
	
		Scanner reader = new Scanner(System.in);
		System.out.println("Please choose the game you want to play. Press 1 for to play Hangman or press 2 to play Wordle :)");
		int number = reader.nextInt();
		
		
		if(number==1){
			System.out.println("You will now play Hangman");
			Hangman();
		}
		else if(number==2){
			System.out.println("You will now play Wordle");
			wordle();
		}
		else System.out.println("You don't get to play a game");
		
	}
	public static void Hangman(){
		Scanner bleh = new Scanner(System.in);
		System.out.println("Welcome to the Hangman Game! :D Please insert the word you want to be guessed.");
		String word = bleh.next();
		System.out.println("Please guess a letter of the word");
		word = word.toUpperCase();
		Hangman.runGame(word);
	}
	public static void wordle(){
		Scanner bleh = new Scanner(System.in);
		System.out.println("Welcome to the Wordle game ! :D Let's see if you can guess the word successfully ;)");
		String word = wordle.generateWord();
		wordle.runGame(word);
	}
}